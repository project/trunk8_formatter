-- SUMMARY --

The Trunk8 Module provides a display field formatter so that site builders
can easily apply the Trunk8 jQuery plugin to their text fields.

For more information on Trunk8, visit the website at:
  http://jrvis.com/trunk8/

To view the Trunk8 source code, visit the repository on Github:
  https://github.com/rviscomi/trunk8

Visit the project page:
  https://www.drupal.org/project/trunk8_formatter

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/trunk8_formatter


-- REQUIREMENTS --

Field
  Core module

-- INSTALLATION --

* Download the latest version of Trunk8 using the following link and
  place it in /libraries.
  The file trunk8.js should be located at /libraries/trunk8/trunk8.js
    https://github.com/rviscomi/trunk8

* Install as usual, see https://www.drupal.org/node/1897420
  for further information.

-- USAGE --
* Visit the "Manage Display" task for your content time.
  E.g. /admin/structure/types/manage/article/display
* Under "Format" for the field, select "Trunk8"
* Change the settings to configure the number of lines to trim to and
  what to use as a fill
  - Default number of lines is 1
  - Default fill is the ellipsis: ...

Current maintainers:
* Chris Rockwell (chrisrockwell) - http://drupal.org/user/448158
